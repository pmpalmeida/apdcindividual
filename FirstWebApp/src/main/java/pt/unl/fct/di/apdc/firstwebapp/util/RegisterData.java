package pt.unl.fct.di.apdc.firstwebapp.util;

public class RegisterData {

	public String username;
	public String password;
	public String confirmation;
	public String email;
	public String role;
	public String phone;
	public String cp;
	public String locality;
	public String address;
	public String compAddress;

	public RegisterData() {

	}

	public RegisterData(String username, String password, String confirmation, String email, String phone, String cp,
			String locality, String address, String compAddress, String role) {
		this.username = username;
		this.password = password;
		this.confirmation = confirmation;
		this.email = email;
		this.phone = phone;
		this.locality = locality;
		this.cp = cp;
		this.address = address;
		this.compAddress = compAddress;
		if (role == null)
			this.role = "user";
		else
			this.role = role;
	}

	private boolean nonEmptyField(String field) {
		return field != null && !field.isEmpty();
	}

	public boolean validRegistration() {
		return nonEmptyField(username) && nonEmptyField(password) && nonEmptyField(confirmation) && nonEmptyField(email)
				&& nonEmptyField(address) && nonEmptyField(cp) && nonEmptyField(locality) && nonEmptyField(phone)
				&& email.contains("@") && password.equals(confirmation) && phone.length() == 9 && password.length() >= 5
				&& cp.contains("-");
	}
}

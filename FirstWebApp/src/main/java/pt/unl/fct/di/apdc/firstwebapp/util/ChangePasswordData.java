package pt.unl.fct.di.apdc.firstwebapp.util;

public class ChangePasswordData {

	public String password;
	public String newpassword;
	public String confirmation;

	public ChangePasswordData() {

	}

	public ChangePasswordData(String password, String newpassword, String confirmation) {
		this.password = password;
		this.newpassword = newpassword;
		this.confirmation = confirmation;
	}

	private boolean nonEmptyField(String field) {
		return field != null && !field.isEmpty();
	}

	public boolean validArguments() {
		return nonEmptyField(password) && nonEmptyField(newpassword) && nonEmptyField(confirmation)
				&& newpassword.equals(confirmation) && newpassword.length() >= 5;
	}
}

package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.util.AuthToken;
import pt.unl.fct.di.apdc.firstwebapp.util.ChangePasswordData;
import pt.unl.fct.di.apdc.firstwebapp.util.LoginData;

@Path("/login")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LoginResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public LoginResource() {
	} // Nothing to be done here...

	@POST
	@Path("/v2")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLoginV2(LoginData data, @Context HttpServletRequest request, @Context HttpHeaders headers) {
		LOG.info("Attempt to login user: " + data.email);

		Transaction txn = datastore.beginTransaction();
		Key userKey = KeyFactory.createKey("User", data.email);
		Entity ustats = null;
		try {
			Entity user = datastore.get(userKey);

			// Obtain the user login statistics
			Query ctrQuery = new Query("UserStats").setAncestor(userKey);
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
			if (results.isEmpty()) {
				ustats = new Entity("UserStats", user.getKey());
				ustats.setProperty("user_stats_logins", 0L);
				ustats.setProperty("user_stats_failed", 0L);
			} else {
				ustats = results.get(0);
			}

			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
				// Password correct

				// Construct the logs
				Entity log = new Entity("UserLog", user.getKey());
				log.setProperty("user_login_ip", request.getRemoteAddr());
				log.setProperty("user_login_host", request.getRemoteHost());
				log.setProperty("user_login_latlon", headers.getHeaderString("X-AppEngine-CityLatLong"));
				log.setProperty("user_login_city", headers.getHeaderString("X-AppEngine-City"));
				log.setProperty("user_login_country", headers.getHeaderString("X-AppEngine-Country"));
				log.setProperty("user_login_time", new Date());
				// Get the user statistics and updates it
				ustats.setProperty("user_stats_logins", 1L + (long) ustats.getProperty("user_stats_logins"));
				ustats.setProperty("user_stats_failed", 0L);
				ustats.setProperty("user_stats_last", new Date());

				// Batch operation
				List<Entity> logs = Arrays.asList(log, ustats);
				datastore.put(txn, logs);
				txn.commit();

				// Return token
				AuthToken token = new AuthToken(data.email);
				LOG.info("User '" + data.email + "' logged in successfully.");
				return Response.ok(this.g.toJson(token)).build();
			} else {
				// Incorrect password
				ustats.setProperty("user_stats_failed", 1L + (long) ustats.getProperty("user_stats_failed"));
				datastore.put(txn, ustats);
				txn.commit();

				LOG.warning("Wrong password for email: " + data.email);
				return Response.status(Status.FORBIDDEN).build();
			}
		} catch (EntityNotFoundException e) {
			// Email does not exist
			LOG.warning("Failed login attempt for email: " + data.email);
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}

	}

	@DELETE
	@Path("/logout")
	public Response logout(@Context UriInfo uriInfo) {
		String email = uriInfo.getQueryParameters().getFirst("email");
		String tokenID = uriInfo.getQueryParameters().getFirst("tokenID");

		LOG.info("Attempt to logout user: " + email);

		try {
			Key userKey = KeyFactory.createKey("User", email);
			Key tokenKey = KeyFactory.createKey("tokenID", tokenID);

			if (datastore.get(userKey).getProperty("user_email").equals(email)) {
				datastore.delete(tokenKey);
				LOG.info("User '" + email + "' logged out successfully.");
				return Response.ok(this.g.toJson(true)).build();
			}

			return Response.status(Response.Status.FORBIDDEN).build();
		} catch (EntityNotFoundException e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@POST
	@Path("/user")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response checkUsernameAvailable(LoginData data) {

		Key userKey = KeyFactory.createKey("User", data.email);
		try {
			Entity user = datastore.get(userKey);
			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -1);
				Date yesterday = cal.getTime();

				// Obtain the user login statistics
				Filter propertyFilter = new FilterPredicate("user_login_time", FilterOperator.GREATER_THAN_OR_EQUAL,
						yesterday);
				Query ctrQuery = new Query("UserLog").setAncestor(KeyFactory.createKey("User", data.email))
						.setFilter(propertyFilter).addSort("user_login_time", SortDirection.DESCENDING);
				ctrQuery.addProjection(new PropertyProjection("user_login_time", Date.class));
				ctrQuery.addProjection(new PropertyProjection("user_login_ip", String.class));
				List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withLimit(3));

				/*
				 * List<Date> loginDates = new ArrayList(); for(Entity userlog:results) {
				 * loginDates.add((Date) userlog.getProperty("user_login_time")); }
				 */ return Response.ok(this.g.toJson(results)).build();

			} else {
				LOG.warning("Wrong password for email: " + data.email);
				return Response.status(Status.FORBIDDEN).build();
			}
		} catch (EntityNotFoundException e) {
			// User does not exist
			LOG.warning("Failed login attempt for email: " + data.email);
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("/changePassword")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response changePassword(ChangePasswordData data, @Context UriInfo uriInfo) {
		String email = uriInfo.getQueryParameters().getFirst("email");
		String tokenID = uriInfo.getQueryParameters().getFirst("tokenID");
		String pwd = data.password;
		String newPwd = data.newpassword;

		LOG.info("Attempt to change password for user: " + email);

		if (!data.validArguments()) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}

		Key userKey = KeyFactory.createKey("User", email);
		try {
			Entity user = datastore.get(userKey);
			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(pwd))) {
				user.setProperty("user_pwd", DigestUtils.sha512Hex(newPwd));
				datastore.put(user);
				LOG.info("Password changed successfully for user " + email);
				return Response.ok(this.g.toJson(true)).build();
			}
			return Response.status(Status.FORBIDDEN).build();
		} catch (EntityNotFoundException e) {
		}
		return Response.status(Status.BAD_REQUEST).build();
	}

	@GET
	@Path("/getZipCode")
	@Produces({ "application/json;charset=utf-8" })
	public Response getUserCP(@Context UriInfo uriInfo) {
		String email = uriInfo.getQueryParameters().getFirst("email");

		Key userKey = KeyFactory.createKey("User", email);
		try {
			Entity user = datastore.get(userKey);
			if (user.getProperty("user_email").equals(email)) {
				String i = (String) user.getProperty("user_cp");
				return Response.ok(this.g.toJson(i)).build();
			}
			return Response.status(Response.Status.FORBIDDEN).entity("You can't do that").build();
		} catch (EntityNotFoundException e) {
		}
		return Response.status(Response.Status.BAD_REQUEST).build();
	}

	@GET
	@Path("/getProfile")
	@Produces({ "application/json;charset=utf-8" })
	public Response getProfile(@Context UriInfo uriInfo) {
		String email = uriInfo.getQueryParameters().getFirst("email");
		LOG.info("Got request to return user '" + email + "' profile.");

		Key userKey = KeyFactory.createKey("User", email);
		try {
			Entity user = datastore.get(userKey);
			if (user.getProperty("user_email").equals(email)) {
				Entity tempUser = new Entity("User");
				tempUser.setUnindexedProperty("username", user.getProperty("user_username"));
				tempUser.setUnindexedProperty("email", user.getProperty("user_email"));
				tempUser.setUnindexedProperty("phone", user.getProperty("user_phone"));
				tempUser.setUnindexedProperty("address", user.getProperty("user_address"));
				tempUser.setUnindexedProperty("compAddress", user.getProperty("user_compAddress"));
				tempUser.setUnindexedProperty("cp", user.getProperty("user_cp"));
				tempUser.setUnindexedProperty("locality", user.getProperty("user_locality"));
				tempUser.setUnindexedProperty("createdIn", user.getProperty("user_creation_time"));
				return Response.ok(this.g.toJson(tempUser)).build();
			}
			return Response.status(Response.Status.FORBIDDEN).entity("You can't do that").build();
		} catch (EntityNotFoundException e) {
		}
		return Response.status(Response.Status.BAD_REQUEST).build();
	}

	@GET
	@Path("/listUsers")
	@Produces({ "application/json;charset=utf-8" })
	public Response listUsers(@Context UriInfo uriInfo) {
		String tokenID = uriInfo.getQueryParameters().getFirst("tokenID");
		LOG.info("Got request to list all users.");

			Query q = new Query("User");
			PreparedQuery pq = datastore.prepare(q);
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			Entity temp = new Entity("User");
			for (Entity result : pq.asIterable()) {
				String username = (String) result.getProperty("user_username");
				String role = (String) result.getProperty("user_role");
				Date creationTime = (Date) result.getProperty("user_creation_time");
				temp.setUnindexedProperty("role", role);
				temp.setUnindexedProperty("username", username);
				temp.setUnindexedProperty("creationTime", creationTime);
				list.add(temp.getProperties());
			}
			return Response.ok(this.g.toJson(list)).build();
	}
	
	@GET
	@Path("/markAll")
	@Produces({ "application/json;charset=utf-8" })
	public Response markAll(@Context UriInfo uriInfo) {
		String tokenID = uriInfo.getQueryParameters().getFirst("tokenID");
		LOG.info("Got request to list all users.");


			Query q = new Query("User");
			PreparedQuery pq = datastore.prepare(q);
			List<String> list = new ArrayList<String>();
			for (Entity result : pq.asIterable()) {
				list.add((String) result.getProperty("user_cp"));
			}
			return Response.ok(this.g.toJson(list)).build();
	}
}

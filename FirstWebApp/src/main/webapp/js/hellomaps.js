var map;
var bounds;
var geocoder;

function listUsersJS(response) {
	var list = response;
	var table = document.getElementById("myTable");
	for(i = 0; i < list.length; i++) {
		var row = table.insertRow(-1);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		var cell3 = row.insertCell(2);
		cell1.innerHTML = list[i].role;
		cell2.innerHTML = list[i].username; 
		cell3.innerHTML = list[i].creationTime; 
	}
}

function markAllJS(response) {
	var list = response;
	bounds = new google.maps.LatLngBounds();
	var i;
	for(i = 0; i < list.length; i++) {
		geocoder.geocode({ address: list[i]}, function(results, status) {
	        if(status == 'OK') {
	            var marker = new google.maps.Marker({ position: results[0].geometry.location, map: map});
	            bounds.extend(results[0].geometry.location);
	        }
	        else {
	            alert('Geocode was not successful for the following reason: '+status);
	        }
	    });
	}
}

function fillBoundsJS(response) {
	for(i = 0; i < list.length; i++) {
		geocoder.geocode({ address: list[i]}, function(results, status) {
	        if(status == 'OK') {
	            var marker = new google.maps.Marker({ position: results[0].geometry.location, map: map});
	            bounds.extend(results[0].geometry.location);
	        }
	        else {
	            alert('Geocode was not successful for the following reason: '+status);
	        }
	    });
	}
}

function fit() {
	map.fitBounds(bounds);
}

function fillProfile(response) {
	document.getElementById('username').innerHTML = response.propertyMap.username.value;
	document.getElementById('email').innerHTML = response.propertyMap.email.value;
	document.getElementById('phone').innerHTML = response.propertyMap.phone.value;
	document.getElementById('address').innerHTML = response.propertyMap.address.value;
	document.getElementById('compAddress').innerHTML = response.propertyMap.compAddress.value;
	document.getElementById('postal').innerHTML = response.propertyMap.cp.value;
	document.getElementById('locality').innerHTML = response.propertyMap.locality.value;
	document.getElementById('createdIn').innerHTML = response.propertyMap.createdIn.value;
}

function codeAddress(addr) {
    geocoder.geocode({ address: addr}, function(results, status) {
        if(status == 'OK') {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({ position: results[0].geometry.location, map: map});
        }
        else {
            alert('Geocode was not successful for the following reason: '+status);
        }
    });
}

function initMap() 
{
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
    });

    geocoder = new google.maps.Geocoder();

    getZipCode();
    
    document.getElementById('geocode').onclick = function(event) {
        codeAddress(document.getElementById('cp').value);
    }
}

function getProfile(){
    $.ajax({
        type : "GET",
        url : `http://localhost:8080/rest/login/getProfile?email=${localStorage.getItem('email')}&tokenID=${localStorage.getItem('tokenID')}`,
        // contentType : "application/json; charset=utf-8",
        crossDomain : true,
        // dataType : "json",
        success : function(response) {
            if (response) {
            	fillProfile(response);
            } else {
                alert("No response");
            }
        },
        error : function(response) {
            alert("Error: " + response.status);
        },
    });
}

function getZipCode(){
    $.ajax({
        type : "GET",
        url : `http://localhost:8080/rest/login/getZipCode?email=${localStorage.getItem('email')}&tokenID=${localStorage.getItem('tokenID')}`,
        // contentType : "application/json; charset=utf-8",
        crossDomain : true,
        // dataType : "json",
        success : function(response) {
            if (response) {
            	codeAddress(response);
            } else {
                alert("No response");
            }
        },
        error : function(response) {
            alert("Error: " + response.status);
        },
    });
}

function listUsers(){
    $.ajax({
        type : "GET",
        url : `http://localhost:8080/rest/login/listUsers?tokenID=${localStorage.getItem('tokenID')}`,
        // contentType : "application/json; charset=utf-8",
        crossDomain : true,
        // dataType : "json",
        success : function(response) {
            if (response) {
            	listUsersJS(response);
            } else {
                alert("No response");
            }
        },
        error : function(response) {
            alert("Error: " + response.status);
        },
    });
}

function markAll(){
    $.ajax({
        type : "GET",
        url : `http://localhost:8080/rest/login/markAll?tokenID=${localStorage.getItem('tokenID')}`,
        // contentType : "application/json; charset=utf-8",
        crossDomain : true,
        // dataType : "json",
        success : function(response) {
            if (response) {
            	markAllJS(response);
            } else {
                alert("No response");
            }
        },
        error : function(response) {
            alert("Error: " + response.status);
        },
    });
}

function fillBounds(){
    $.ajax({
        type : "GET",
        url : `http://localhost:8080/rest/login/markAll?tokenID=${localStorage.getItem('tokenID')}`,
        // contentType : "application/json; charset=utf-8",
        crossDomain : true,
        // dataType : "json",
        success : function(response) {
            if (response) {
            	fillBoundsJS(response);
            } else {
                alert("No response");
            }
        },
        error : function(response) {
            alert("Error: " + response.status);
        },
    });
}

changePassword = function (event){
    document.getElementById("incorrect").style.visibility = "hidden";
	var data = $('form[name="changepwd"]').jsonify();
    $.ajax({
        type : "POST",
        url : `http://localhost:8080/rest/login/changePassword?email=${localStorage.getItem('email')}&tokenID=${localStorage.getItem('tokenID')}`,
        contentType : "application/json; charset=utf-8",
        crossDomain : true,
        dataType : "json",
        success : function(response) {
            if (response) {
                alert("Successfully changed password!")
            } else {
                alert("No response");
            }
        },
        error : function(response) {
        	if (response.status == "403")
        	    document.getElementById("incorrect").style.visibility = "visible";
        	else
        		alert("Error: " + response.status);
        },
        data: JSON.stringify(data)
    });
    event.preventDefault();
}

window.onload = function() {
    console.log(`Token: ${localStorage.tokenID}`);
    var frms = $('form[name="changepwd"]');
    frms[0].onsubmit = changePassword;
}

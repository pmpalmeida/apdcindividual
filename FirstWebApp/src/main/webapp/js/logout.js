function logout() {
    $.ajax({
        type: "DELETE",
        url: `/rest/login/logout?email=${localStorage.getItem('email')}&tokenID=${localStorage.getItem('tokenID')}`,
        //contentType: "application/json; charset=utf-8",
        crossDomain: true,
        //dataType: "json",
        success: function(response) {
            if (response) {
                localStorage.removeItem('tokenID');
                localStorage.removeItem('email');
                window.location.replace("login.html");
            } else
                alert("ups.")
        },
        error: function(response) {
            alert("Error: " + response.status);
        }
    });
}
captureData = function(event) {
    var data = $('form[name="register"]').jsonify();
    console.log(data);
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/rest/register",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function(response) {
            if (response) {
                localStorage.setItem('tokenID', response.tokenID);
                window.location.replace("login.html");
            } else {
                alert("No response");
            }
        },
        error: function(response) {
            alert("Error: " + response.status);
        },
        data: JSON.stringify(data)
    });

    event.preventDefault();
};

window.onload = function() {
    var frms = $('form[name="register"]');
    frms[0].onsubmit = captureData;
}
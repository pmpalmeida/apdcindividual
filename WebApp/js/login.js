captureData = function(event) {
    console.log($('form[name="login"]'));
    var data = $('form[name="login"]').jsonify();
    console.log(data);
    console.log(JSON.stringify(data));
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/rest/login/v2",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function(response) {
            if (response) {
                // Store token id for later use in localStorage
                localStorage.setItem('tokenID', response.tokenID);
                localStorage.setItem('email', data.email);
                window.location.replace("hellomaps.html");
            } else {
                alert("No response");
            }
        },
        error: function(response) {
            alert("Error: " + response.status);
        },
        data: JSON.stringify(data)
    });

    event.preventDefault();
};

window.onload = function() {
    var frms = $('form[name="login"]'); //var frms = document.getElementsByName("login");
    frms[0].onsubmit = captureData;
}


$("#passwordfield").on("keyup",function(){
    if($(this).val())
        $(".glyphicon-eye-open").show();
    else
        $(".glyphicon-eye-open").hide();
    });
$(".glyphicon-eye-open").mousedown(function(){
                $("#passwordfield").attr('type','text');
            }).mouseup(function(){
                $("#passwordfield").attr('type','password');
            }).mouseout(function(){
                $("#passwordfield").attr('type','password');
            });